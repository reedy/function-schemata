'use strict';

const fs = require( 'fs' );
const path = require( 'path' );
const yaml = require( 'yaml' );

// Have a file size read upper limit of 10 MiB for safety.
const upperfileSizeLimit = 10 * 1024 * 1024;

/**
 * Internal method to load a given file, but limited for safety to 10 MiB
 *
 * @param {string} fileName
 * @param {number} fileSizeLimit
 * @return {string}
 */
function readFileLimited( fileName, fileSizeLimit = upperfileSizeLimit ) {
	// Have a file size read upper limit of 10 MiB for safety.
	fileSizeLimit = Math.min( fileSizeLimit, upperfileSizeLimit );

	// eslint-disable-next-line security/detect-non-literal-fs-filename
	const stats = fs.statSync( fileName );
	if ( stats.size > fileSizeLimit ) {
		throw new Error( 'File ' + fileName + ' is larger than the maximum permitted size, ' + fileSizeLimit + ' bytes' );
	}

	// eslint-disable-next-line security/detect-non-literal-fs-filename
	return fs.readFileSync( fileName, { encoding: 'utf8', start: 0, end: fileSizeLimit } );
}

function dataDir( ...pathComponents ) {
	return path.join(
		path.dirname( path.dirname( path.dirname( __filename ) ) ),
		'data', ...pathComponents );
}

function readYaml( fileName ) {
	return yaml.parse( readFileLimited( fileName ) );
}

function readJSON( fileName ) {
	return JSON.parse( readFileLimited( fileName ) );
}

module.exports = {
	dataDir,
	readFileLimited,
	readJSON,
	readYaml
};
