'use strict';

/**
 * This is a wrapper for the logger provided by service runner.
 * It provides more user-friendly logging APIs and better error
 * signaling for when it is used incorrectly.
 *
 * This is the logger that other scripts in this project will interact with.
 * Usage:
 * const logger = new LoggerWrapper( <somelogger> );
 * logger.log('warn', 'hello this is a message');
 * logger.warn('hello this is also a message');
 */
class LoggerWrapper {
	constructor( logger ) {
		this._logger = logger;
	}

	/**
	 * Logs a message on a given severity level.
	 * Acceptable levels: trace, debug, info, warn, error, and fatal.
	 *
	 * @param {string} level Severity level and components.
	 *     Level options: trace, debug, info, warn, error, and fatal.
	 *     E.g. trace or trace/request.
	 * @param {string} msg A string message for the log.
	 * @param {Object} data Any relevant data for the log entry.
	 */
	log( level, msg, data ) {
		if ( !level || !msg ) {
			// The service runner implementation will just silently no-op
			// in this situation. We want to alert the caller here.
			throw new Error(
				`Incorrect usage of the logger. Both arguments need to be
				present. E.g. logger.log(level, msg). Alternatively you can
				use the logger.level() API.` );
		}
		this._logger.log( level, msg, data );
	}

	/**
	 * Logs a tracing message.
	 *
	 * @param {string} msg Trace message.
	 * @param {Object} data Any relevant data for the log entry.
	 */
	trace( msg, data ) {
		this.log( 'trace', msg, data );
	}

	/**
	 * Logs a debug message.
	 *
	 * @param {string} msg Debug message.
	 * @param {Object} data Any relevant data for the log entry.
	 */
	debug( msg, data ) {
		this.log( 'debug', msg, data );
	}

	/**
	 * Logs a info message.
	 *
	 * @param {string} msg Info message.
	 * @param {Object} data Any relevant data for the log entry.
	 */
	info( msg, data ) {
		this.log( 'info', msg, data );
	}

	/**
	 * Logs a warning message.
	 *
	 * @param {string} msg Warning message.
	 * @param {Object} data Any relevant data for the log entry.
	 */
	warn( msg, data ) {
		this.log( 'warn', msg, data );
	}

	/**
	 * Logs a error message.
	 *
	 * @param {string} msg Error message.
	 * @param {Object} data Any relevant data for the log entry.
	 */
	error( msg, data ) {
		this.log( 'error', msg, data );
	}

	/**
	 * Logs a fatal message.
	 *
	 * @param {string} msg Fatal message.
	 * @param {Object} data Any relevant data for the log entry.
	 */
	fatal( msg, data ) {
		this.log( 'fatal', msg, data );
	}

	/**
	 * Creates a child logger for a sub-component of your application.
	 * This directly wraps its core logger obj's implementation.
	 *
	 * @param {*} args arguments for the child wrapper.
	 * @return {LoggerWrapper} A new logger for the sub-component.
	 */
	child( args ) {
		return new LoggerWrapper( this._logger.child( args ) );
	}
}

module.exports = { LoggerWrapper };
