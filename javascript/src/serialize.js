'use strict';

/** @deprecated */

const {
	avroSchema,
	convertFormattedRequestToVersionedBinary,
	convertWrappedZObjectToVersionedBinary,
	convertZObjectToBinary,
	getWrappedZObjectFromVersionedBinary,
	getZObjectFromBinary,
	convertNormalForBinaryFormat,
	recoverNormalFromBinaryFormat,
	zobjectSchemaV002
} = require( './binaryFormatter.js' );

module.exports = {
	avroSchema,
	convertFormattedRequestToVersionedBinary,
	convertWrappedZObjectToVersionedBinary,
	convertZObjectToBinary,
	getWrappedZObjectFromVersionedBinary,
	getZObjectFromBinary,
	convertNormalForBinaryFormat,
	recoverNormalFromBinaryFormat,
	zobjectSchemaV002
};
